import { Bomb } from './bomb';
import { Boost } from './boost';
import { Engine } from '../index';
import { IEntity } from './typedef';
import { Position, EventName, EventPayload } from '../typedef';

export interface PlayerStats {
  maxHealth: number;
  bombCount: number;
  bombRadius: number;
}

export class Player implements IEntity {
  public entityType = 'PLAYER';

  public stats: PlayerStats = {
    maxHealth: 1,
    bombCount: 1,
    bombRadius: 1,
  };
  public health = 1;

  constructor(
    private engine: Engine,
    public position: Position,
    public id: string = '',
    public nickname: string = '',
    private self = false,
  ) {
    this.engine.terrain.map[position.y][position.x] = this;
    this.engine.renderer.renderPlayer(position, this.self);
  }

  public emitEvent(eventName: EventName, payload: EventPayload) {
    this.engine.emitEvent(eventName, payload);
  }
  private die() {
    // TODO: Implement death animation
    this.engine.terrain.map[this.position.y][this.position.x] = null;
    this.engine.renderer.clearCell(this.position);
  }

  dealDamage() {
    // TODO: Implement damage animation
    this.health -= 1;
    if (this.health === 0) this.die();
  }
  move(direction: 'up' | 'down' | 'left' | 'right'): void {
    let { x, y } = this.position;
    const prevCell = this.engine.terrain.map[y][x];
    if (prevCell && prevCell.entityType === 'BOMB') {
      this.engine.renderer.renderBomb(this.position);
    } else {
      this.engine.terrain.map[this.position.y][this.position.x] = null;
      this.engine.renderer.clearCell(this.position);
    }

    switch (direction) {
      case 'up':
        this.position.y -= 1;
        break;
      case 'down':
        this.position.y += 1;
        break;
      case 'left':
        this.position.x -= 1;
        break;
      case 'right':
        this.position.x += 1;
        break;
    }

    ({ x, y } = this.position);
    const newCell = this.engine.terrain.map[y][x];
    if (newCell && newCell.entityType === 'BOOST') {
      (newCell as Boost).consume(this);
    }
    this.engine.terrain.map[y][x] = this;
    this.engine.renderer.renderPlayer(this.position, this.self);
  }
  putBomb(position: Position): void {
    if (process.env.DEBUG) {
      console.log('Player', this.id, 'put bomb');
    }

    const { x, y } = position;
    this.engine.terrain.map[y][x] = new Bomb(this.engine, this);

    if (x !== this.position.x || y !== this.position.y) {
      this.engine.renderer.renderBomb(position);
    } else {
      // TODO: Render player with bomb
    }
  }
}
