import { Engine } from '@/engine';
import { IEntity } from './typedef';
import { Position, DestroyBoxEvent } from '../typedef';

import { Boost } from './boost';

export class Box implements IEntity {
  public entityType = 'BOX';

  constructor(private engine: Engine, private position: Position) {
    this.engine.renderer.render(position, 'BOX');
  }

  public destroy(event: DestroyBoxEvent) {
    setTimeout(() => {
      this.engine.renderer.clearCell(event.position);
      if (process.env.DEBUG) {
        console.log('Box on position', event.position, 'has been destroyed');
      }

      const boost = event.boostKind
        ? new Boost(this.engine, event.boostKind, event.position)
        : null;
      this.engine.terrain.map[this.position.y][this.position.x] = boost;
    },         300);
  }
}
