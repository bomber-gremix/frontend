import { Engine } from '@/engine';
import { Position } from '@/engine/typedef';
import { IEntity } from './typedef';

export class Block implements IEntity {
  public entityType = 'BLOCK';

  constructor(engine: Engine, position: Position) {
    engine.renderer.render(position, 'BLOCK');
  }
}
