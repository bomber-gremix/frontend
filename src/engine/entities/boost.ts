import { Engine } from '@/engine';
import { Position } from '@/engine/typedef';

import { Player } from './player';
import { IEntity } from './typedef';

export type BoostKind = 'bombRadius' | 'bombCount' | 'health';

export class Boost implements IEntity {
  public entityType = 'BOOST';

  constructor(
    private engine: Engine,
    public kind: BoostKind,
    position: Position,
  ) {
    this.engine.renderer.renderBoost(kind, position);
  }

  public consume(player: Player) {
    switch (this.kind) {
      case 'bombRadius':
        player.stats.bombRadius += 1;
        break;
      case 'bombCount':
        player.stats.bombCount += 1;
        break;
      case 'health':
        if (player.health === player.stats.maxHealth) {
          player.stats.maxHealth += 1;
        }
        player.health += 1;
        break;
    }
  }
}
