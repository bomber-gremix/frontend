import { Engine } from '@/engine';
import { Player } from './player';
import { IEntity } from './typedef';
import { Position } from '../typedef';

let visit: boolean[][] | null = null;
export class Bomb implements IEntity {
  public entityType = 'BOMB';

  private position: Position;
  public radius: number;

  constructor(private engine: Engine, private player: Player) {
    this.radius = this.player.stats.bombRadius;
    this.position = { ...this.player.position };
  }

  private static directions = [[1, 0], [-1, 0], [0, 1], [0, -1]];

  public explode(
    x: number = this.position.x,
    y: number = this.position.y,
    d: number = 0,
    delta: number[] | null = null,
  ) {
    if (d > this.radius) return;
    if (visit == null) {
      visit = Array(this.engine.terrain.width)
        .fill(null)
        .map(() => Array(this.engine.terrain.height).fill(false));
    }
    if (visit[y][x]) return;
    visit[y][x] = true;
    const curPos = { x, y };
    const currentCell = this.engine.terrain.map[y][x];
    if (currentCell !== null && currentCell.entityType === 'BOX') {
      this.engine.renderer.renderFire(curPos);
    }
    if (currentCell === null) {
      this.engine.renderer.renderFire(curPos);
      setTimeout(() => {
        this.engine.renderer.clearCell(curPos);
      },         300);
    }
    // TODO: Adjust clear timeout
    setTimeout(() => {
      if (
        (currentCell && currentCell.entityType !== 'PLAYER') ||
        currentCell === null
      ) {
        this.engine.renderer.clearCell(this.position);
      }
    },         300);

    let shouldSpread = true;
    const entity: IEntity | null = this.engine.terrain.map[y][x];

    if (entity) {
      if (entity.entityType === 'BLOCK' || entity.entityType === 'BOX') {
        shouldSpread = false;
      }
    }

    if (!shouldSpread) return;

    const directions = delta === null ? Bomb.directions : [delta];
    for (const delta of directions) {
      const dX = x + delta[0];
      const dY = y + delta[1];

      if (dX < 0 || dX >= this.engine.terrain.width) continue;
      if (dY < 0 || dY >= this.engine.terrain.height) continue;

      this.explode(dX, dY, d + 1, delta);
    }

    if (d === 0) visit = null;
  }
}
