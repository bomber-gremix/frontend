import { Box } from './entities/box';
import { Block } from './entities/block';
import { Engine } from './index';
import { IEntity } from './entities/typedef';
import { Position } from './typedef';

export type TerrainCell = 'SPAWNPOINT' | 'BOX' | 'BLOCK' | null;
export type TerrainSchema = TerrainCell[][];

export interface ITerrain {
  map: (IEntity | null)[][];
  width: number;
  height: number;
  zone: Position[];
  availableSpawnPoints: { [key: string]: boolean };

  isInSafeZone(position: Position): boolean;
}

export class Terrain implements ITerrain {
  map: (IEntity | null)[][];
  width = 11;
  height = 9;
  zone: Position[] = [{ x: 0, y: 0 }];
  availableSpawnPoints: { [key: string]: boolean } = {};

  constructor(engine: Engine, schema: TerrainSchema) {
    this.height = schema.length;
    this.width = schema[0].length;
    this.zone.push({ x: this.width - 1, y: this.height - 1 });

    this.map = Array(this.height)
      .fill(null)
      .map((_, y) =>
        Array(this.width)
          .fill(null)
          .map((_, x) => {
            switch (schema[y][x]) {
              case 'BLOCK':
                return new Block(engine, { x, y });
              case 'BOX':
                return new Box(engine, { x, y });
              default:
                return null;
            }
          }),
      );
  }

  public isInSafeZone(position: Position) {
    return (
      position.x >= this.zone[0].x &&
      position.x <= this.zone[1].x &&
      position.y >= this.zone[0].y &&
      position.y <= this.zone[1].y
    );
  }
}
