import { Renderer } from '@/renderer';
import { Connection } from '@/network/connection';
import * as messages from '@/network/messages';

import { Bomb } from './entities/bomb';
import { Boost } from './entities/boost';
import { Player } from './entities/player';
import { IEntity } from './entities/typedef';
import { Terrain, TerrainSchema } from './terrain';
import {
  IEngine,
  EventName,
  Position,
  EventPayload,
  DestroyBoxEvent,
} from './typedef';
import { Box } from './entities/box';

export type GameState = (IEntity | null)[][];

export class Engine implements IEngine {
  public self: Player | null = null;
  public terrain: Terrain;
  public players: { [key: string]: Player } = {};
  private terrainSchema: TerrainSchema = require('@/renderer/mockMap.json');

  constructor(public conn: Connection, public renderer: Renderer) {
    this.terrain = new Terrain(this, this.terrainSchema);
    document.onkeydown = this.handleKeypress;
    document.onkeyup = () => (this.currentKey = null);
  }

  private currentKey: string | null = null;
  private moveInterval: any | null = null;
  private handleKeypress = (event: KeyboardEvent) => {
    this.currentKey = event.key;
    if (this.currentKey === ' ') {
      this.self!.emitEvent('putBomb', {
        player: this.self!,
        position: this.self!.position,
      });
      return;
    }

    this.move();
    this.moveInterval = setInterval(this.move, 300);
  }

  private directions = {
    ArrowUp: 'up',
    ArrowDown: 'down',
    ArrowLeft: 'left',
    ArrowRight: 'right',
  };
  private move() {
    if (!this.currentKey) {
      clearInterval(this.moveInterval);
      return;
    }
    if (this.directions[this.currentKey]) {
      this.emitEvent('move', {
        direction: this.directions[this.currentKey],
        player: this.self,
      } as any);
    }
  }

  public emitEvent(eventName: EventName, payload: EventPayload) {
    this.conn.sendEvent(eventName, payload);
  }
  public handleEvent(eventName: EventName, payload: EventPayload) {
    let x: number;
    let y: number;
    switch (eventName) {
      case 'spawn':
        const spawnEvent = payload as messages.ISpawnEvent;
        this.players[spawnEvent.player.id] = new Player(
          this,
          spawnEvent.position,
          spawnEvent.player.id,
          spawnEvent.player.nickname,
          !!spawnEvent.player.self,
        );
        if (spawnEvent.player.self) {
          this.self = this.players[spawnEvent.player.id];
        }
        break;
      case 'move':
        const moveEvent = payload as messages.IMoveEvent;
        if (process.env.DEBUG) {
          console.log(
            'Moving player',
            moveEvent.player.id,
            moveEvent.direction,
          );
        }
        this.players[moveEvent.player.id].move(moveEvent.direction as any);
        break;
      case 'putBomb':
        const putBombEvent = payload as messages.IPutBombEvent;
        this.players[putBombEvent.player.id].putBomb(putBombEvent.position);
        break;
      case 'destroyBox':
        const destroyBoxEvent = payload as DestroyBoxEvent;
        ({ x, y } = destroyBoxEvent.position);
        (this.terrain.map[y][x] as Box).destroy(destroyBoxEvent);
        break;
      case 'bombExplode':
        ({ x, y } = payload as Position);
        // TODO: Render explosion
        (this.terrain.map[y][x] as Bomb).explode();
        break;
      case 'damage':
        this.players[(payload as Player).id].dealDamage();
        break;
    }
  }
}
