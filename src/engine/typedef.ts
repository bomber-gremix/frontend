import { Player } from './entities/player';
import { ITerrain } from './terrain';
import { BoostKind } from './entities/boost';

export interface Position {
  x: number;
  y: number;
}

export interface MoveEvent {
  player: Player;
  direction: 'up' | 'down' | 'left' | 'right';
}
export interface SpawnEvent {
  player: Player;
  position: Position;
}
export type PutBombEvent = SpawnEvent;
export interface DestroyBoxEvent {
  position: Position;
  boostKind: BoostKind | null;
}

export type EventName =
  | 'start'
  | 'move'
  | 'spawn'
  | 'damage'
  | 'putBomb'
  | 'destroyBox'
  | 'bombExplode';
export type EventPayload =
  | Player
  | Position
  | MoveEvent
  | SpawnEvent
  | PutBombEvent
  | DestroyBoxEvent
  | null;

export interface IEngine {
  terrain: ITerrain;

  emitEvent(eventName: EventName, payload: EventPayload): void;
  handleEvent(eventName: EventName, payload: EventPayload): void;
}
