import * as PIXI from 'pixi.js';

import { createButton } from './components/button';

export class StartPage {
  public startCallBack: Function = () => {};

  private root = new PIXI.Container();
  constructor(private app: PIXI.Application) {
    const background = PIXI.Sprite.from('/assets/background.jpg');
    this.root.addChild(background);

    const page = PIXI.Sprite.from('/assets/start/page.png');
    page.x = 338.5;
    page.y = 201;
    this.root.addChild(page);

    const button = createButton(
      {
        default: PIXI.Texture.from('/assets/start/button.png'),
        hover: PIXI.Texture.from('/assets/start/buttonHover.png'),
      },
      () => this.startCallBack(),
    );
    button.x = 511;
    button.y = 543;

    this.root.addChild(button);
  }

  render = () => {
    this.app.stage.removeChildren();
    this.app.stage.addChild(this.root);
  }
}
