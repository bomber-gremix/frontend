import * as PIXI from 'pixi.js';
import { createButton } from './components/button';

export class SelectHeroPage {
  public exitCallback = () => {};
  public startCallback = (hero: 'gremix' | 'cherry' | 'sky') => {};

  private currentPlayer: 'gremix' | 'cherry' | 'sky' | null = null;
  private textures = {
    gremix: {
      default: PIXI.Texture.from('/assets/heroes/gremix.png'),
      hover: PIXI.Texture.from('/assets/heroes/gremixHover.png'),
      chosen: PIXI.Texture.from('/assets/heroes/gremixChosen.png'),
    },
    cherry: {
      default: PIXI.Texture.from('/assets/heroes/cherry.png'),
      hover: PIXI.Texture.from('/assets/heroes/cherryHover.png'),
      chosen: PIXI.Texture.from('/assets/heroes/cherryChosen.png'),
    },
    sky: {
      default: PIXI.Texture.from('/assets/heroes/sky.png'),
      hover: PIXI.Texture.from('/assets/heroes/skyHover.png'),
      chosen: PIXI.Texture.from('/assets/heroes/skyChosen.png'),
    },
  };
  private currentTextures = {
    gremix: {
      default: PIXI.Texture.from('/assets/heroes/gremix.png'),
      hover: PIXI.Texture.from('/assets/heroes/gremixHover.png'),
    },
    cherry: {
      default: PIXI.Texture.from('/assets/heroes/cherry.png'),
      hover: PIXI.Texture.from('/assets/heroes/cherryHover.png'),
    },
    sky: {
      default: PIXI.Texture.from('/assets/heroes/sky.png'),
      hover: PIXI.Texture.from('/assets/heroes/skyHover.png'),
    },
  };

  private heroButtons = {
    gremix: createButton(this.currentTextures.gremix, () =>
      this.setCurrentPlayer('gremix'),
    ),
    cherry: createButton(this.currentTextures.cherry, () =>
      this.setCurrentPlayer('cherry'),
    ),
    sky: createButton(this.currentTextures.sky, () =>
      this.setCurrentPlayer('sky'),
    ),
  };

  private setCurrentPlayer(currentPlayer: 'gremix' | 'cherry' | 'sky') {
    if (currentPlayer === this.currentPlayer) return;
    this.currentPlayer = currentPlayer;

    Object.entries(this.currentTextures).forEach(([player]) => {
      if (player === currentPlayer) {
        this.currentTextures[player].default = this.textures[player].chosen;
        this.currentTextures[player].hover = this.textures[player].chosen;
        this.heroButtons[player].texture = this.textures[player].chosen;
      } else {
        this.currentTextures[player].default = this.textures[player].default;
        this.currentTextures[player].hover = this.textures[player].hover;
        this.heroButtons[player].texture = this.textures[player].default;
      }
    });
  }

  private root = new PIXI.Container();
  constructor(private app: PIXI.Application) {
    const background = PIXI.Sprite.from('/assets/background.jpg');
    // background.anchor.set(0.5);
    // background.x = this.app.screen.width / 2;
    // background.y = this.app.screen.height / 2;
    this.root.addChild(background);

    const page = PIXI.Sprite.from('/assets/heroes/page.png');
    page.x = 328;
    page.y = 138.5;
    background.addChild(page);

    const container = new PIXI.Container();
    page.addChild(container);

    const exitButton = createButton(
      {
        default: PIXI.Texture.from('/assets/heroes/exitButton.png'),
        hover: PIXI.Texture.from('/assets/heroes/exitButtonHover.png'),
      },
      () => this.exitCallback(),
    );
    exitButton.x = 898;
    exitButton.y = 186;
    this.root.addChild(exitButton);

    this.heroButtons.gremix.x = 561;
    this.heroButtons.gremix.y = 348;
    this.root.addChild(this.heroButtons.gremix);

    this.heroButtons.cherry.x = 393;
    this.heroButtons.cherry.y = 348;
    this.root.addChild(this.heroButtons.cherry);

    this.heroButtons.sky.x = 729;
    this.heroButtons.sky.y = 348;
    this.root.addChild(this.heroButtons.sky);

    const startButton = createButton(
      {
        default: PIXI.Texture.from('/assets/heroes/start.png'),
        hover: PIXI.Texture.from('/assets/heroes/startHover.png'),
      },
      () => {
        if (!this.currentPlayer) return;
        this.startCallback(this.currentPlayer);
      },
    );
    startButton.x = 584;
    startButton.y = 605;
    this.root.addChild(startButton);
  }

  render = () => {
    this.app.stage.removeChildren();
    this.app.stage.addChild(this.root);
  }
}
