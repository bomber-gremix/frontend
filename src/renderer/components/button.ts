import * as PIXI from 'pixi.js';

export interface ButtonTextures {
  default: PIXI.Texture;
  hover: PIXI.Texture;
}

export function createButton(
  textures: ButtonTextures,
  onClick?: () => void,
): PIXI.Sprite {
  const button = new PIXI.Sprite(textures.default);

  let isDown = false;
  let isOver = false;

  button.buttonMode = true;
  button.interactive = true;

  const onButtonUp = () => {
    isDown = false;
    button.texture = isOver ? textures.hover : textures.default;
  };

  button
    .on('pointerdown', () => {
      isDown = true;
      if (onClick) onClick();
    })
    .on('pointerup', onButtonUp)
    .on('pointerupoutside', onButtonUp)
    .on('pointerover', () => {
      isOver = true;
      if (isDown) return;
      button.texture = textures.hover;
    })
    .on('pointerout', () => {
      isOver = false;
      if (isDown) return;
      button.texture = textures.default;
    });

  return button;
}
