import * as PIXI from 'pixi.js';

const mapWidth = 11;
const mapHeight = 9;

const map = require('./mockMap.json');

export type Entity =
  | 'BOMB'
  | 'BLOCK'
  | 'BOX'
  | 'BOOST_RADIUS'
  | 'BOOST_COUNT'
  | 'BOOST_HEALTH'
  | 'PLAYER'
  | 'ALIEN'
  | 'SPAWNPOINT'
  | 'FIRE'
  | 'EMPTY';

export class GamePage {
  private root = new PIXI.Container();
  private innerMap = new PIXI.Container();
  private textures = {
    BOMB: '/assets/game/bomb.png',
    BLOCK: '/assets/game/block.png',
    BOX: '/assets/game/box.png',
    BOOST_RADIUS: '/assets/game/boostRadius.png',
    BOOST_COUNT: '/assets/game/boostCount.png',
    BOOST_HEALTH: '/assets/game/boostHealth.png',
    PLAYER: '/assets/game/player.png',
    ALIEN: '/assets/game/alien.png',
    SPAWNPOINT: undefined,
    FIRE: '/assets/game/fire.png',
    EMPTY: undefined,
  };
  private terrain = Array(mapHeight)
    .fill(null)
    .map((_, i: number) =>
      Array(mapWidth)
        .fill(null)
        .map((_, j: number) => {
          const container = new PIXI.Container();
          container.width = container.height = 53;
          container.x -= 53 * 14 - j * 53;
          container.y -= 53 * 11 - i * 53;
          this.innerMap.addChild(container);
          return container;
        }),
    );
  private terrainContainer = PIXI.Sprite.from('/assets/game/page.png');

  constructor(private app: PIXI.Application) {
    const background = PIXI.Sprite.from('/assets/background.jpg');
    background.anchor.set(0.5);
    background.x = this.app.screen.width / 2;
    background.y = this.app.screen.height / 2;
    this.root.addChild(background);

    this.terrainContainer.anchor.set(0.5);
    background.addChild(this.terrainContainer);

    // border top
    const topBorder = new PIXI.Container();
    topBorder.y -= 320;
    topBorder.x -= 292.5;
    for (let i = 0; i < 11; i += 1) {
      const borderBlock = PIXI.Sprite.from('/assets/game/borderBlock.png');
      borderBlock.x += i * 53;
      topBorder.addChild(borderBlock);
    }
    background.addChild(topBorder);

    // border left
    const leftBorder = new PIXI.Container();
    leftBorder.y -= 267;
    leftBorder.x -= 345.5;
    for (let i = 0; i < 10; i += 1) {
      const borderBlock = PIXI.Sprite.from('/assets/game/borderBlock.png');
      borderBlock.y += i * 53;
      leftBorder.addChild(borderBlock);
    }
    background.addChild(leftBorder);

    // border right
    const rightBorder = new PIXI.Container();
    rightBorder.y -= 267;
    rightBorder.x += 292.5;
    for (let i = 0; i < 10; i += 1) {
      const borderBlock = PIXI.Sprite.from('/assets/game/borderBlock.png');
      borderBlock.y += i * 53;
      rightBorder.addChild(borderBlock);
    }
    background.addChild(rightBorder);
    const bottomBorder = new PIXI.Container();
    bottomBorder.y += 210;
    bottomBorder.x -= 292.5;
    for (let i = 0; i < 11; i += 1) {
      const borderBlock = PIXI.Sprite.from('/assets/game/borderBlock.png');
      borderBlock.x += i * 53;
      bottomBorder.addChild(borderBlock);
    }
    background.addChild(bottomBorder);

    this.innerMap.x = 477.5;
    this.innerMap.y = 343;
    this.terrainContainer.addChild(this.innerMap);
  }

  public render = (x: number, y: number, entity: Entity) => {
    this.terrain[y][x].removeChildren();
    if (!this.textures[entity as string]) {
      return;
    }
    const newEntity = PIXI.Sprite.from(this.textures[entity as string]);
    newEntity.anchor.set(0.5);

    const scaleFactor = newEntity.width / newEntity.height;
    if (newEntity.width > newEntity.height && newEntity.width > 53) {
      newEntity.width = 53;
      newEntity.height = 53 / scaleFactor;
    } else if (newEntity.height > 53) {
      newEntity.width = 53 * scaleFactor;
      newEntity.height = 53;
    }

    this.terrain[y][x].addChild(newEntity);
  }

  start = (hero: 'gremix' | 'cherry' | 'sky') => {
    this.textures.PLAYER = `/assets/game/${hero}.png`;
    this.app.stage.removeChildren();
    this.app.stage.addChild(this.root);
  }
}
