import * as PIXI from 'pixi.js';

import { Engine } from '@/engine';
import { Position } from '@/engine/typedef';
import { BoostKind } from '@/engine/entities/boost';
import { StartPage } from './startPage';
import { SelectHeroPage } from './selectHeroPage';
import { GamePage, Entity } from './game';

export class Renderer {
  public app = new PIXI.Application({
    width: 1280,
    height: 800,
  });
  public startCallback = () => {};

  private gamePage = new GamePage(this.app);
  private startPage = new StartPage(this.app);
  private selectHeroPage = new SelectHeroPage(this.app);

  public renderPlayer(position: Position, self: boolean) {
    if (process.env.DEBUG) {
      console.log('Rendering', self ? 'player' : 'alien', 'on', position);
    }
    this.render(position, self ? 'PLAYER' : 'ALIEN');
  }
  public renderBomb(position: Position) {
    this.render(position, 'BOMB');
  }
  public clearCell(position: Position) {
    this.render(position, 'EMPTY');
  }
  public renderFire(position: Position) {
    this.render(position, 'FIRE');
  }

  private boosts: { [key: string]: Entity } = {
    bombRadius: 'BOOST_RADIUS',
    bombCount: 'BOOST_COUNT',
    health: 'BOOST_HEALTH',
  };
  public renderBoost(kind: BoostKind, position: Position) {
    this.render(position, this.boosts[kind]);
  }

  public render = (position: Position, entity: Entity) =>
    this.gamePage.render(position.x, position.y, entity)

  constructor(private engine: Engine) {
    this.startPage.startCallBack = this.selectHeroPage.render;
    this.selectHeroPage.exitCallback = this.startPage.render;
    this.selectHeroPage.startCallback = (hero: 'gremix' | 'cherry' | 'sky') => {
      this.gamePage.start(hero);
      this.startCallback();
    };
    this.startPage.render();
  }
}
