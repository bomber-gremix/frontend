import vjs from '@zolbooo/vjs';
import { Renderer } from './renderer';
import { Connection } from './network/connection';

import './main.css';

function getParameter(name) {
  if (
    (name = new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)').exec(
      location.search
    ))
  )
    return decodeURIComponent(name[1]);
}

document.addEventListener('DOMContentLoaded', () => {
  const renderer = new Renderer();
  document.getElementById('root').append(renderer.app.view);

  renderer.startCallback = () => {
    const connection = new Connection(renderer);
    connection.connect().then(() => connection.connectToLobby(getParameter('l')));
  };
});
