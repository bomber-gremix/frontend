import { Engine } from '@/engine';
import { Renderer } from '@/renderer';
import { EventName, EventPayload } from '@/engine/typedef';

import * as messages from './messages';

export class Connection {
  private ws = new WebSocket(process.env.SERVER_ADDRESS!, 'game-protocol');
  private engine?: Engine;
  private $connecting: Promise<void>;

  constructor(public renderer: Renderer) {
    this.ws.binaryType = 'arraybuffer';
    this.$connecting = new Promise(
      (resolve: () => void) =>
        (this.ws.onopen = () => {
          if (process.env.DEBUG) console.log('Connected to server');
          resolve();
        }),
    );
  }

  public connect() {
    return this.$connecting;
  }
  public connectToLobby(lobbyName: string) {
    if (process.env.DEBUG) console.log('Connecting to lobby:', lobbyName);
    const message = messages.ClientHello.encode(
      messages.ClientHello.fromObject({
        lobbyName,
        nickname: 'test',
      }),
    ).finish();
    this.ws.send(message);
    this.engine = new Engine(this, this.renderer);
    this.ws.onmessage = this.handleMessage;
  }

  private handleMessage = (messageEvent: MessageEvent) => {
    const payload: Uint8Array = new Uint8Array(
      messageEvent.data as ArrayBuffer,
    );
    const event: messages.Event = messages.Event.decode(payload);

    if (process.env.DEBUG) console.log('Recieved message:', event);

    let eventPayload: any;
    switch (event.eventName) {
      case 'spawn':
        eventPayload = event.spawnEvent;
        break;
      case 'move':
        eventPayload = event.moveEvent;
        break;
      case 'putBomb':
        eventPayload = event.putBombEvent;
        break;
      case 'bombExplode':
        eventPayload = event.position;
        break;
      case 'destroyBox':
        eventPayload = event.destroyBoxEvent;
        break;
      case 'damage':
        eventPayload = event.damageEvent;
        break;
    }
    if (eventPayload) {
      this.engine!.handleEvent(event.eventName as any, eventPayload);
    }
  }
  sendEvent(eventName: EventName, payload: EventPayload) {
    let event: messages.Event | undefined;
    switch (eventName) {
      case 'move':
        event = messages.Event.fromObject({
          eventName: 'move',
          moveEvent: payload as messages.IMoveEvent,
        });
        break;
      case 'putBomb':
        event = messages.Event.fromObject({
          eventName: 'putBomb',
          putBombEvent: payload as messages.IPutBombEvent,
        });
        break;
    }
    if (event) this.ws.send(messages.Event.encode(event).finish());
  }
}
