/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/light");

var $root = ($protobuf.roots["default"] || ($protobuf.roots["default"] = new $protobuf.Root()))
.addJSON({
  Player: {
    fields: {
      id: {
        rule: "required",
        type: "string",
        id: 1
      },
      nickname: {
        rule: "required",
        type: "string",
        id: 2
      },
      self: {
        type: "bool",
        id: 3
      }
    }
  },
  Position: {
    fields: {
      x: {
        rule: "required",
        type: "uint32",
        id: 1
      },
      y: {
        rule: "required",
        type: "uint32",
        id: 2
      }
    }
  },
  MoveEvent: {
    fields: {
      player: {
        rule: "required",
        type: "Player",
        id: 1
      },
      direction: {
        rule: "required",
        type: "string",
        id: 2
      }
    }
  },
  SpawnEvent: {
    fields: {
      player: {
        rule: "required",
        type: "Player",
        id: 1
      },
      position: {
        rule: "required",
        type: "Position",
        id: 2
      }
    }
  },
  PutBombEvent: {
    fields: {
      player: {
        rule: "required",
        type: "Player",
        id: 1
      },
      position: {
        rule: "required",
        type: "Position",
        id: 2
      }
    }
  },
  DestroyBoxEvent: {
    fields: {
      position: {
        rule: "required",
        type: "Position",
        id: 1
      },
      boostKind: {
        type: "string",
        id: 2
      }
    }
  },
  ClientHello: {
    fields: {
      nickname: {
        rule: "required",
        type: "string",
        id: 1
      },
      lobbyName: {
        rule: "required",
        type: "string",
        id: 2
      }
    }
  },
  ServerHello: {
    fields: {
      id: {
        rule: "required",
        type: "string",
        id: 1
      }
    }
  },
  Event: {
    oneofs: {
      payload: {
        oneof: [
          "damageEvent",
          "position",
          "moveEvent",
          "spawnEvent",
          "putBombEvent",
          "destroyBoxEvent"
        ]
      }
    },
    fields: {
      eventName: {
        rule: "required",
        type: "string",
        id: 1
      },
      damageEvent: {
        type: "Player",
        id: 2
      },
      position: {
        type: "Position",
        id: 3
      },
      moveEvent: {
        type: "MoveEvent",
        id: 4
      },
      spawnEvent: {
        type: "SpawnEvent",
        id: 5
      },
      putBombEvent: {
        type: "PutBombEvent",
        id: 6
      },
      destroyBoxEvent: {
        type: "DestroyBoxEvent",
        id: 7
      }
    }
  }
});

module.exports = $root;
