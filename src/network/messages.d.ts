import * as $protobuf from "protobufjs";
/** Properties of a Player. */
export interface IPlayer {

    /** Player id */
    id: string;

    /** Player nickname */
    nickname: string;

    /** Player self */
    self?: (boolean|null);
}

/** Represents a Player. */
export class Player implements IPlayer {

    /**
     * Constructs a new Player.
     * @param [properties] Properties to set
     */
    constructor(properties?: IPlayer);

    /** Player id. */
    public id: string;

    /** Player nickname. */
    public nickname: string;

    /** Player self. */
    public self: boolean;

    /**
     * Creates a new Player instance using the specified properties.
     * @param [properties] Properties to set
     * @returns Player instance
     */
    public static create(properties?: IPlayer): Player;

    /**
     * Encodes the specified Player message. Does not implicitly {@link Player.verify|verify} messages.
     * @param message Player message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IPlayer, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified Player message, length delimited. Does not implicitly {@link Player.verify|verify} messages.
     * @param message Player message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IPlayer, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a Player message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns Player
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Player;

    /**
     * Decodes a Player message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns Player
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Player;

    /**
     * Verifies a Player message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a Player message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns Player
     */
    public static fromObject(object: { [k: string]: any }): Player;

    /**
     * Creates a plain object from a Player message. Also converts values to other types if specified.
     * @param message Player
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: Player, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this Player to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a Position. */
export interface IPosition {

    /** Position x */
    x: number;

    /** Position y */
    y: number;
}

/** Represents a Position. */
export class Position implements IPosition {

    /**
     * Constructs a new Position.
     * @param [properties] Properties to set
     */
    constructor(properties?: IPosition);

    /** Position x. */
    public x: number;

    /** Position y. */
    public y: number;

    /**
     * Creates a new Position instance using the specified properties.
     * @param [properties] Properties to set
     * @returns Position instance
     */
    public static create(properties?: IPosition): Position;

    /**
     * Encodes the specified Position message. Does not implicitly {@link Position.verify|verify} messages.
     * @param message Position message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IPosition, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified Position message, length delimited. Does not implicitly {@link Position.verify|verify} messages.
     * @param message Position message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IPosition, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a Position message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns Position
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Position;

    /**
     * Decodes a Position message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns Position
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Position;

    /**
     * Verifies a Position message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a Position message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns Position
     */
    public static fromObject(object: { [k: string]: any }): Position;

    /**
     * Creates a plain object from a Position message. Also converts values to other types if specified.
     * @param message Position
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: Position, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this Position to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a MoveEvent. */
export interface IMoveEvent {

    /** MoveEvent player */
    player: IPlayer;

    /** MoveEvent direction */
    direction: string;
}

/** Represents a MoveEvent. */
export class MoveEvent implements IMoveEvent {

    /**
     * Constructs a new MoveEvent.
     * @param [properties] Properties to set
     */
    constructor(properties?: IMoveEvent);

    /** MoveEvent player. */
    public player: IPlayer;

    /** MoveEvent direction. */
    public direction: string;

    /**
     * Creates a new MoveEvent instance using the specified properties.
     * @param [properties] Properties to set
     * @returns MoveEvent instance
     */
    public static create(properties?: IMoveEvent): MoveEvent;

    /**
     * Encodes the specified MoveEvent message. Does not implicitly {@link MoveEvent.verify|verify} messages.
     * @param message MoveEvent message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IMoveEvent, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified MoveEvent message, length delimited. Does not implicitly {@link MoveEvent.verify|verify} messages.
     * @param message MoveEvent message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IMoveEvent, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a MoveEvent message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns MoveEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): MoveEvent;

    /**
     * Decodes a MoveEvent message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns MoveEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): MoveEvent;

    /**
     * Verifies a MoveEvent message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a MoveEvent message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns MoveEvent
     */
    public static fromObject(object: { [k: string]: any }): MoveEvent;

    /**
     * Creates a plain object from a MoveEvent message. Also converts values to other types if specified.
     * @param message MoveEvent
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: MoveEvent, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this MoveEvent to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a SpawnEvent. */
export interface ISpawnEvent {

    /** SpawnEvent player */
    player: IPlayer;

    /** SpawnEvent position */
    position: IPosition;
}

/** Represents a SpawnEvent. */
export class SpawnEvent implements ISpawnEvent {

    /**
     * Constructs a new SpawnEvent.
     * @param [properties] Properties to set
     */
    constructor(properties?: ISpawnEvent);

    /** SpawnEvent player. */
    public player: IPlayer;

    /** SpawnEvent position. */
    public position: IPosition;

    /**
     * Creates a new SpawnEvent instance using the specified properties.
     * @param [properties] Properties to set
     * @returns SpawnEvent instance
     */
    public static create(properties?: ISpawnEvent): SpawnEvent;

    /**
     * Encodes the specified SpawnEvent message. Does not implicitly {@link SpawnEvent.verify|verify} messages.
     * @param message SpawnEvent message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: ISpawnEvent, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified SpawnEvent message, length delimited. Does not implicitly {@link SpawnEvent.verify|verify} messages.
     * @param message SpawnEvent message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: ISpawnEvent, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a SpawnEvent message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns SpawnEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): SpawnEvent;

    /**
     * Decodes a SpawnEvent message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns SpawnEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): SpawnEvent;

    /**
     * Verifies a SpawnEvent message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a SpawnEvent message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns SpawnEvent
     */
    public static fromObject(object: { [k: string]: any }): SpawnEvent;

    /**
     * Creates a plain object from a SpawnEvent message. Also converts values to other types if specified.
     * @param message SpawnEvent
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: SpawnEvent, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this SpawnEvent to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a PutBombEvent. */
export interface IPutBombEvent {

    /** PutBombEvent player */
    player: IPlayer;

    /** PutBombEvent position */
    position: IPosition;
}

/** Represents a PutBombEvent. */
export class PutBombEvent implements IPutBombEvent {

    /**
     * Constructs a new PutBombEvent.
     * @param [properties] Properties to set
     */
    constructor(properties?: IPutBombEvent);

    /** PutBombEvent player. */
    public player: IPlayer;

    /** PutBombEvent position. */
    public position: IPosition;

    /**
     * Creates a new PutBombEvent instance using the specified properties.
     * @param [properties] Properties to set
     * @returns PutBombEvent instance
     */
    public static create(properties?: IPutBombEvent): PutBombEvent;

    /**
     * Encodes the specified PutBombEvent message. Does not implicitly {@link PutBombEvent.verify|verify} messages.
     * @param message PutBombEvent message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IPutBombEvent, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified PutBombEvent message, length delimited. Does not implicitly {@link PutBombEvent.verify|verify} messages.
     * @param message PutBombEvent message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IPutBombEvent, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a PutBombEvent message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns PutBombEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PutBombEvent;

    /**
     * Decodes a PutBombEvent message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns PutBombEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PutBombEvent;

    /**
     * Verifies a PutBombEvent message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a PutBombEvent message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns PutBombEvent
     */
    public static fromObject(object: { [k: string]: any }): PutBombEvent;

    /**
     * Creates a plain object from a PutBombEvent message. Also converts values to other types if specified.
     * @param message PutBombEvent
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: PutBombEvent, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this PutBombEvent to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a DestroyBoxEvent. */
export interface IDestroyBoxEvent {

    /** DestroyBoxEvent position */
    position: IPosition;

    /** DestroyBoxEvent boostKind */
    boostKind?: (string|null);
}

/** Represents a DestroyBoxEvent. */
export class DestroyBoxEvent implements IDestroyBoxEvent {

    /**
     * Constructs a new DestroyBoxEvent.
     * @param [properties] Properties to set
     */
    constructor(properties?: IDestroyBoxEvent);

    /** DestroyBoxEvent position. */
    public position: IPosition;

    /** DestroyBoxEvent boostKind. */
    public boostKind: string;

    /**
     * Creates a new DestroyBoxEvent instance using the specified properties.
     * @param [properties] Properties to set
     * @returns DestroyBoxEvent instance
     */
    public static create(properties?: IDestroyBoxEvent): DestroyBoxEvent;

    /**
     * Encodes the specified DestroyBoxEvent message. Does not implicitly {@link DestroyBoxEvent.verify|verify} messages.
     * @param message DestroyBoxEvent message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IDestroyBoxEvent, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified DestroyBoxEvent message, length delimited. Does not implicitly {@link DestroyBoxEvent.verify|verify} messages.
     * @param message DestroyBoxEvent message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IDestroyBoxEvent, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a DestroyBoxEvent message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns DestroyBoxEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): DestroyBoxEvent;

    /**
     * Decodes a DestroyBoxEvent message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns DestroyBoxEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): DestroyBoxEvent;

    /**
     * Verifies a DestroyBoxEvent message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a DestroyBoxEvent message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns DestroyBoxEvent
     */
    public static fromObject(object: { [k: string]: any }): DestroyBoxEvent;

    /**
     * Creates a plain object from a DestroyBoxEvent message. Also converts values to other types if specified.
     * @param message DestroyBoxEvent
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: DestroyBoxEvent, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this DestroyBoxEvent to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a ClientHello. */
export interface IClientHello {

    /** ClientHello nickname */
    nickname: string;

    /** ClientHello lobbyName */
    lobbyName: string;
}

/** Represents a ClientHello. */
export class ClientHello implements IClientHello {

    /**
     * Constructs a new ClientHello.
     * @param [properties] Properties to set
     */
    constructor(properties?: IClientHello);

    /** ClientHello nickname. */
    public nickname: string;

    /** ClientHello lobbyName. */
    public lobbyName: string;

    /**
     * Creates a new ClientHello instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ClientHello instance
     */
    public static create(properties?: IClientHello): ClientHello;

    /**
     * Encodes the specified ClientHello message. Does not implicitly {@link ClientHello.verify|verify} messages.
     * @param message ClientHello message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IClientHello, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified ClientHello message, length delimited. Does not implicitly {@link ClientHello.verify|verify} messages.
     * @param message ClientHello message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IClientHello, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a ClientHello message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ClientHello
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): ClientHello;

    /**
     * Decodes a ClientHello message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ClientHello
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): ClientHello;

    /**
     * Verifies a ClientHello message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a ClientHello message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ClientHello
     */
    public static fromObject(object: { [k: string]: any }): ClientHello;

    /**
     * Creates a plain object from a ClientHello message. Also converts values to other types if specified.
     * @param message ClientHello
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: ClientHello, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this ClientHello to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a ServerHello. */
export interface IServerHello {

    /** ServerHello id */
    id: string;
}

/** Represents a ServerHello. */
export class ServerHello implements IServerHello {

    /**
     * Constructs a new ServerHello.
     * @param [properties] Properties to set
     */
    constructor(properties?: IServerHello);

    /** ServerHello id. */
    public id: string;

    /**
     * Creates a new ServerHello instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ServerHello instance
     */
    public static create(properties?: IServerHello): ServerHello;

    /**
     * Encodes the specified ServerHello message. Does not implicitly {@link ServerHello.verify|verify} messages.
     * @param message ServerHello message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IServerHello, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified ServerHello message, length delimited. Does not implicitly {@link ServerHello.verify|verify} messages.
     * @param message ServerHello message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IServerHello, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a ServerHello message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ServerHello
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): ServerHello;

    /**
     * Decodes a ServerHello message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ServerHello
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): ServerHello;

    /**
     * Verifies a ServerHello message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a ServerHello message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ServerHello
     */
    public static fromObject(object: { [k: string]: any }): ServerHello;

    /**
     * Creates a plain object from a ServerHello message. Also converts values to other types if specified.
     * @param message ServerHello
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: ServerHello, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this ServerHello to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of an Event. */
export interface IEvent {

    /** Event eventName */
    eventName: string;

    /** Event damageEvent */
    damageEvent?: (IPlayer|null);

    /** Event position */
    position?: (IPosition|null);

    /** Event moveEvent */
    moveEvent?: (IMoveEvent|null);

    /** Event spawnEvent */
    spawnEvent?: (ISpawnEvent|null);

    /** Event putBombEvent */
    putBombEvent?: (IPutBombEvent|null);

    /** Event destroyBoxEvent */
    destroyBoxEvent?: (IDestroyBoxEvent|null);
}

/** Represents an Event. */
export class Event implements IEvent {

    /**
     * Constructs a new Event.
     * @param [properties] Properties to set
     */
    constructor(properties?: IEvent);

    /** Event eventName. */
    public eventName: string;

    /** Event damageEvent. */
    public damageEvent?: (IPlayer|null);

    /** Event position. */
    public position?: (IPosition|null);

    /** Event moveEvent. */
    public moveEvent?: (IMoveEvent|null);

    /** Event spawnEvent. */
    public spawnEvent?: (ISpawnEvent|null);

    /** Event putBombEvent. */
    public putBombEvent?: (IPutBombEvent|null);

    /** Event destroyBoxEvent. */
    public destroyBoxEvent?: (IDestroyBoxEvent|null);

    /** Event payload. */
    public payload?: ("damageEvent"|"position"|"moveEvent"|"spawnEvent"|"putBombEvent"|"destroyBoxEvent");

    /**
     * Creates a new Event instance using the specified properties.
     * @param [properties] Properties to set
     * @returns Event instance
     */
    public static create(properties?: IEvent): Event;

    /**
     * Encodes the specified Event message. Does not implicitly {@link Event.verify|verify} messages.
     * @param message Event message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IEvent, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified Event message, length delimited. Does not implicitly {@link Event.verify|verify} messages.
     * @param message Event message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IEvent, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes an Event message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns Event
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Event;

    /**
     * Decodes an Event message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns Event
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Event;

    /**
     * Verifies an Event message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates an Event message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns Event
     */
    public static fromObject(object: { [k: string]: any }): Event;

    /**
     * Creates a plain object from an Event message. Also converts values to other types if specified.
     * @param message Event
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: Event, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this Event to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}
