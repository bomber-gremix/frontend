const nicepack = require('@nicepack/core');
const DotenvPlugin = require('dotenv-webpack');

module.exports = nicepack(
  config => {
    config.babel.presets.push('@babel/typescript');
    config.babel.plugins.push([
      '@babel/plugin-transform-react-jsx',
      {
        pragma: 'vjs.h',
        pragmaFrag: 'vjs.Fragment',
      },
    ]);
  },
  config => config.plugins.push(new DotenvPlugin()),
);

module.exports.devServer.overlay = true;
module.exports.devServer.host = '0.0.0.0';
module.exports.entry = './src/index.js';
module.exports.resolve.extensions.push('.ts', '.tsx');
module.exports.module.rules[0].test = /(\.jsx?|\.tsx?)$/;
